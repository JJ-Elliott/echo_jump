﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EquipmentBase : MonoBehaviour
{
    protected PlayerCombat owner;

    public abstract void OnUseStart();
    public abstract void OnUseHold(float holdTime, Vector3 mouseVector);
    public abstract void OnUseRelease(float holdTime, Vector3 mouseVector);
    public virtual void Equip(PlayerCombat equiper)
    {
        owner = equiper;
    }
}
