﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkArrow : EquipmentBase
{
    bool inHand = true;
    public float chargeTime = 5.0f;
    public float maxLaunch = 10.0f;
    public float minLaunch = 10.0f;
    Rigidbody rigid;
    Collider col;
    public float grabRadius = 5.0f;
    float grabWait = 1.0f;
    bool canGrab = true;
    float drawStep = 0.1f;
    int maxSteps = 1000;
    LineRenderer lr;

    private void Start()
    {
        GetRefs();
        rigid.isKinematic = true;
        col.enabled = false;
        lr = GetComponent<LineRenderer>();
    }

    IEnumerator EnableGrab()
    {
        yield return new WaitForSeconds(grabWait);
        canGrab = true;
    }
    private void Update()
    {
        if(!inHand && canGrab && owner != null && (transform.position - owner.transform.position).magnitude < grabRadius)
        {
            Grab();
        }
    }
    void GetRefs()
    {
        rigid = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();

    }
    public override void OnUseHold(float holdTime, Vector3 mouseVector)
    {
        if (inHand)
        {
            transform.forward = mouseVector;
            transform.position = owner.transform.position + mouseVector * 1.0f;
            DrawTrajectory(Mathf.Lerp(minLaunch, maxLaunch, Mathf.Clamp01(holdTime / chargeTime)), mouseVector);
        }
    }

    public override void OnUseRelease(float holdTime, Vector3 mouseVector)
    {
        if (inHand)
        {
            float force = Mathf.Lerp(minLaunch,maxLaunch,Mathf.Clamp01(holdTime / chargeTime));
            rigid.isKinematic = false;
            transform.parent = null;
            col.enabled = true;
            rigid.AddForce(mouseVector * force);
            inHand = false;
            canGrab = false;
            StartCoroutine(EnableGrab());
            lr.enabled = false;
        }
        else
        {
            owner.transform.position = transform.position + Vector3.up;
            Grab();
        }

    }

    
    public override void OnUseStart()
    {
        
    }

    public override void Equip(PlayerCombat equiper)
    {
        if(rigid == null)
        {
            GetRefs();
        }

        base.Equip(equiper);
        Grab();
    }

    void Grab()
    {
        transform.parent = owner.transform;
        transform.position = owner.transform.position;
        rigid.isKinematic = true;
        col.enabled = false;
        inHand = true;
    }

    Vector3 GetTrajectoryPosition(Vector3 launchVector , float launchSpeed , float t)
    {
        return owner.transform.position + (launchSpeed * t * launchVector) + (Physics.gravity * Mathf.Pow(t ,2) * 100);
    }

    Vector3 GetTrajectorySlope(Vector3 launchVector, float launchSpeed, float t)
    {
        return (launchVector + (Physics.gravity * t * 100));
    }

    void DrawTrajectory(float launchSpeed, Vector3 launchVector)
    {
        List<Vector3> drawPoints = new List<Vector3>();

        for(int i = 0; i < maxSteps; i++)
        {
            var point = GetTrajectoryPosition(launchVector, launchSpeed, drawStep * i);
            var dir = GetTrajectorySlope(launchVector, launchSpeed, drawStep * i);

            RaycastHit hit;
            Physics.Raycast(point, dir.normalized, out hit, drawStep * dir.magnitude);
            if (hit.transform)
            {
                drawPoints.Add(hit.point);
                break;
            }
            drawPoints.Add(point);
        }

        lr.enabled = true;
        lr.positionCount = drawPoints.Count;
        lr.SetPositions(drawPoints.ToArray());
    }
}
