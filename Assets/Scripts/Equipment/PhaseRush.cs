﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PhaseRush : EquipmentBase
{
    Vector3 setPos;
    LineRenderer lr;
    bool active = false;
    public float damage = 10;
    public GameObject makerPrefab;

    Dictionary<Transform, Transform> targetMarkers;
    private void Start()
    {
        lr = GetComponent<LineRenderer>();
        lr.positionCount = 2;
        targetMarkers = new Dictionary<Transform, Transform>();
    }

    public override void OnUseHold(float holdTime, Vector3 mouseVector)
    {
        
    }

    public override void OnUseRelease(float holdTime, Vector3 mouseVector)
    {
        

        if (!active)
        {
            setPos = owner.transform.position;
            active = true;
            lr.SetPosition(0, owner.transform.position);
            lr.SetPosition(1, setPos);
            lr.enabled = true;
        } else
        {
            foreach (var target in Physics.SphereCastAll(owner.transform.position, 1, setPos - owner.transform.position, (setPos - owner.transform.position).magnitude))
            {
                Damageable dmg = target.transform.gameObject.GetComponent<Damageable>();
                if (dmg != null)
                {
                    dmg.Damage(damage);
                }
            }
            owner.transform.position = setPos;
            active = false;
            lr.enabled = false;
            ClearMarkers();
        }
    }

    void ClearMarkers()
    {
        foreach (var marker in targetMarkers.Values)
        {
            Destroy(marker.gameObject);
        }
        targetMarkers.Clear();
    }
    public override void OnUseStart()
    {
        
    }

    public override void Equip(PlayerCombat equiper)
    {
        base.Equip(equiper);
        transform.position = owner.transform.position;
    }

    private void Update()
    {
        if (active)
        {
            lr.SetPosition(0, owner.transform.position);
            lr.SetPosition(1, setPos);
            ClearMarkers();
            foreach (var target in Physics.SphereCastAll(owner.transform.position, 1, (setPos - owner.transform.position).normalized, (setPos - owner.transform.position).magnitude))
            {
                Damageable dmg = target.transform.gameObject.GetComponent<Damageable>();
                if (dmg != null)
                {
                    if (targetMarkers.ContainsKey(dmg.transform)){
                        targetMarkers[dmg.transform].position = dmg.transform.position + Vector3.up;
                    } else
                    {
                        targetMarkers[dmg.transform] = Instantiate(makerPrefab).transform;
                        targetMarkers[dmg.transform].position = dmg.transform.position + Vector3.up;

                    }
                }
            }
        }
        
    }
}
