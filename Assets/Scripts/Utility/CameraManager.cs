﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]

public class CameraManager : MonoBehaviour
{

    public float springForce = 0.1f;

    public Transform target;
    Vector3 offset;
    Vector3 targetPosition;
    Camera cam;
    public static Vector3 mouseVector;
    
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        if(target == null)
        {
            offset = transform.position;
        } else
        {
            offset = transform.position - target.position;
        }
    }


    void UpdateTarget()
    {
        if (target == null)
        {
            targetPosition = transform.position;
        }

        targetPosition = target.position + offset;
    }

    void DoMovement()
    {
        Vector3 toTarget = targetPosition - transform.position;


        transform.position += toTarget.sqrMagnitude * springForce * toTarget.normalized;
        
    }

    void UpdateMouseVector()
    {
        Vector2 targetPosition;
        if(target != null)
        {
            targetPosition = cam.WorldToScreenPoint(target.position);
        } else
        {
            targetPosition = new Vector2(Screen.currentResolution.width /2 , Screen.currentResolution.height /2);
        }

        Vector2 mousePos = Input.mousePosition;

        Vector2 screenMouseVector = mousePos - targetPosition;

        screenMouseVector.Normalize();

        mouseVector.x = screenMouseVector.x;
        mouseVector.z = screenMouseVector.y;
        mouseVector.y = 0;

    }
    // Update is called once per frame
    void Update()
    {
        UpdateTarget();
        DoMovement();
        UpdateMouseVector();
    }
}
