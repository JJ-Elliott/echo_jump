﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour
{
    CharacterController cc;
    public float maxSpeed = 6.0f;

    Vector3 moveVector;

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        GetMoveVector();
        DoMovement();
    }

    void GetMoveVector()
    {
        moveVector = Vector3.zero;
        moveVector.x = Input.GetAxis("Horizontal");
        moveVector.z = Input.GetAxis("Vertical");
        moveVector = moveVector.normalized;
    }

    void DoMovement()
    {
        cc.SimpleMove(moveVector * maxSpeed);
        if(moveVector != Vector3.zero)
        {
            transform.forward = moveVector;

        }
    }
}
