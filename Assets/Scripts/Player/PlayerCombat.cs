﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public EquipmentBase equipmentPrimary, equipmentSecondary;

    float primaryHoldTime = 0;
    float secondaryHoldTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        EquipmentBase[] equips = GetComponentsInChildren<EquipmentBase>();
        for(int i = 0; i < equips.Length; i++)
        {
            if(i == 0)
            {
                equipmentPrimary = equips[i];
                equipmentPrimary.Equip(this);
            } else if (i == 1)
            {
                equipmentSecondary = equips[i];
                equipmentSecondary.Equip(this);
            } else
            {
                print("Too many equips on player... " + equips[i].transform.name + " will not be equipped");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(equipmentPrimary != null)
        {
            if (Input.GetButtonDown("Primary"))
            {
                equipmentPrimary.OnUseStart();
                primaryHoldTime = 0;
            }
            else if (Input.GetButton("Primary"))
            {
                primaryHoldTime += Time.deltaTime;
                equipmentPrimary.OnUseHold(primaryHoldTime, CameraManager.mouseVector);
            }
            else if (Input.GetButtonUp("Primary"))
            {
                equipmentPrimary.OnUseRelease(primaryHoldTime, CameraManager.mouseVector);
            }
        }

        if (equipmentSecondary != null)
        {
            if (Input.GetButtonDown("Secondary"))
            {
                equipmentSecondary.OnUseStart();
                secondaryHoldTime = 0;
            }
            else if (Input.GetButton("Secondary"))
            {
                secondaryHoldTime += Time.deltaTime;
                equipmentSecondary.OnUseHold(secondaryHoldTime, CameraManager.mouseVector);
            }
            else if (Input.GetButtonUp("Secondary"))
            {
                equipmentSecondary.OnUseRelease(secondaryHoldTime, CameraManager.mouseVector);
            }
        }
    }
}
