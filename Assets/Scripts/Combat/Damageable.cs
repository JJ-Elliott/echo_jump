﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageEvent : UnityEvent<float> { }

public abstract class Damageable : MonoBehaviour
{
    public abstract void Damage(float dmg);
    public DamageEvent OnDamage;
}
