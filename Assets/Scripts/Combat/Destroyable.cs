﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Destroyable : Damageable
{
    public float totalHealth, currentHealth;
    public DamageEvent OnDeath;
    public Image healthBar;

    public override void Damage(float dmg)
    {
        currentHealth -= dmg;
        currentHealth = Mathf.Max(currentHealth, 0);

        if(OnDamage != null)
        {
            OnDamage.Invoke(dmg);
        }
        if (OnDeath != null && currentHealth == 0)
        {
            OnDeath.Invoke(dmg);
        }

        Destroy(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        currentHealth = totalHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if(healthBar != null)
        {
            if (currentHealth != totalHealth)
            {
                healthBar.enabled = true;
                healthBar.fillAmount = currentHealth / totalHealth;
            }
            else
            {
                healthBar.enabled = false;
            }
        }
        
    }
}
